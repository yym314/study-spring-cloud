package com.yao.spring.cloud.weather.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.yao.spring.cloud.weather.vo.City;
import com.yao.spring.cloud.weather.vo.WeatherResponse;

@Component
public class DataClientFallback implements DataClient {

	@Override
	public List<City> list() {
		List<City> cityList = null;
		City city = new City();
		city.setCityID("101280601");
		city.setCityName("深圳");
		cityList = Arrays.asList(city);
		
		return cityList;
	}

	@Override
	public WeatherResponse getDataByCityID(String cityID) {
		
		return null;
	}

}
