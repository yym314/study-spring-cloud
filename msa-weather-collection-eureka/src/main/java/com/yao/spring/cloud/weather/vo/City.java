package com.yao.spring.cloud.weather.vo;

/**
 * 
 * @Description: 城市信息
 * @author yaoym
 * @date 2018年3月23日 上午9:14:30
 * @version V1.0
 */

public class City {

	private String cityID;

	private String cityName;

	private String cityCode;

	private String province;

	public String getCityID() {
		return cityID;
	}

	public void setCityID(String cityID) {
		this.cityID = cityID;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@Override
	public String toString() {
		return "City [cityID=" + cityID + ", cityName=" + cityName + ", cityCode=" + cityCode + ", province=" + province
				+ "]";
	}

}
