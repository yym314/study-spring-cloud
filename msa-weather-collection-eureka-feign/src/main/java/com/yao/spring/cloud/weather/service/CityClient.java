/**   
* @Title: CityClient.java 
* @Package com.yao.spring.cloud.weather.service 
* @Description: TODO(用一句话描述该文件做什么) 
* @author yaoym
* @date 2018年3月28日 上午10:09:42 
* @version V1.0   
*/
package com.yao.spring.cloud.weather.service;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.yao.spring.cloud.weather.vo.City;

@FeignClient("msa-weather-city-eureka")
public interface CityClient {

	@GetMapping("/cities/list")
	List<City> list();
}
