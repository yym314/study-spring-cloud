package com.yao.spring.cloud.weather.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.yao.spring.cloud.weather.vo.WeatherResponse;

@FeignClient("msa-weather-data-eureka")
public interface WeatherDataClient {

	@GetMapping("/weather/cityId/{cityId}")
	WeatherResponse getDataByCityID(@PathVariable("cityId") String cityID);
}
