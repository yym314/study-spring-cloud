package com.yao.spring.cloud.weather.vo;

import java.io.Serializable;
import java.util.List;

/**
 * 天气信息
 * 
 * @Description: TODO(用一句话描述该文件做什么)
 * @author yaoym
 * @date 2018年3月20日 下午2:58:59
 * @version V1.0
 */
public class Weather implements Serializable {

	private static final long serialVersionUID = 1L;

	private String city;
	private String aqi;
	private String ganmao;
	private String wendu;
	private Yeaterday yesterday;
	private List<Forecast> forecast;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAqi() {
		return aqi;
	}

	public void setAqi(String aqi) {
		this.aqi = aqi;
	}

	public String getGanmao() {
		return ganmao;
	}

	public void setGanmao(String ganmao) {
		this.ganmao = ganmao;
	}

	public String getWendu() {
		return wendu;
	}

	public void setWendu(String wendu) {
		this.wendu = wendu;
	}

	public Yeaterday getYesterday() {
		return yesterday;
	}

	public void setYesterday(Yeaterday yesterday) {
		this.yesterday = yesterday;
	}

	public List<Forecast> getForecast() {
		return forecast;
	}

	public void setForecast(List<Forecast> forecast) {
		this.forecast = forecast;
	}

}
