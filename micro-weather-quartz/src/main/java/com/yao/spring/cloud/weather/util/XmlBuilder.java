/**   
* @Title: XmlBuilder.java 
* @Package com.yao.spring.cloud.weather.util 
* @Description: TODO(用一句话描述该文件做什么) 
* @author yaoym
* @date 2018年3月23日 上午9:20:33 
* @version V1.0   
*/
package com.yao.spring.cloud.weather.util;

import java.io.Reader;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

/**
 * @Description: Xml 生成
 * @author yaoym
 * @date 2018年3月23日 上午9:20:33
 * @version V1.0
 */
public class XmlBuilder {

	/**
	 * 将xml转为相应的pojo
	 * 
	 * @Description:
	 * @since 2018年3月23日 上午9:22:58
	 * @author yaoym
	 * @param cls
	 * @param xmlStr
	 * @return
	 * @throws Exception
	 */
	public static Object xmlStrToObject(Class<?> cls, String xmlStr) throws Exception {
		Object xmlObject = null;
		Reader reader = null;

		JAXBContext context = JAXBContext.newInstance(cls);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		reader = new StringReader(xmlStr);
		xmlObject = unmarshaller.unmarshal(reader);

		if (reader != null) {
			reader.close();
		}

		return xmlObject;
	}
}
