package com.yao.spring.cloud.weather.service;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.yao.spring.cloud.weather.vo.City;
import com.yao.spring.cloud.weather.vo.WeatherResponse;

@FeignClient("msa-weather-eureka-client-zuul")
public interface DataClient {
	/**
	 * 获取城市列表
	 * 
	 * @since 2018年3月28日 下午2:18:08
	 * @author yaoym
	 * @return
	 */
	@GetMapping("/city/cities/list")
	List<City> list();

	/**
	 * 根据城市id获取天气数据
	 * 
	 * @since 2018年3月28日 下午2:18:08
	 * @author yaoym
	 * @return
	 */
	@GetMapping("/data/weather/cityId/{cityId}")
	WeatherResponse getDataByCityID(@PathVariable("cityId") String cityID);
}
