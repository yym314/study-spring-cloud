package com.yao.spring.cloud;

/**
 * 继承 Thread类
 * 
 * @Description:
 * @author yaoym
 * @date 2018年3月29日 上午9:38:49
 * @version V1.0
 */
public class MyThreadWithImplement implements Runnable {

	private static int tickets = 10;
	
	@Override
	public synchronized void run() {
		for (int i = 0; i < 100; i++) {
			if (tickets > 0) {
				System.out.println(Thread.currentThread().getName()+"----卖出票：" + tickets--);  
			}
		}
	}
	
	public static void main(String[] args) {
		MyThreadWithImplement myRunnable = new MyThreadWithImplement();
        Thread thread1 = new Thread(myRunnable, "窗口一");  
        Thread thread2 = new Thread(myRunnable, "窗口二");  
        Thread thread3 = new Thread(myRunnable, "窗口三");  

		thread1.start();
		thread2.start();
		thread3.start();
	}
}
