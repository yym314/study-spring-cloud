package com.yao.spring.cloud;

/**
 * 继承 Thread类
 * 
 * @Description:
 * @author yaoym
 * @date 2018年3月29日 上午9:38:49
 * @version V1.0
 */
public class MyThreadWithExtends extends Thread {

	private static int tickets = 10;
	
	@Override
	public void run() {
		for (int i = 0; i <= 100; i++) {
			if (tickets > 0) {
				System.out.println(Thread.currentThread().getName()+"--卖出票：" + tickets--);  
			}
		}
	}
	
	public static void main(String[] args) {
		MyThreadWithExtends thread1 = new MyThreadWithExtends();
		MyThreadWithExtends thread2 = new MyThreadWithExtends();
		MyThreadWithExtends thread3 = new MyThreadWithExtends();

		thread1.start();
		thread2.start();
		thread3.start();
	}
}
