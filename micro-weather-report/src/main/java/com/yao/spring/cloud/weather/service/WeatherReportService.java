/**   
* @Title: WeatherReportService.java 
* @Package com.yao.spring.cloud.weather.service 
* @Description: TODO(用一句话描述该文件做什么) 
* @author yaoym
* @date 2018年3月23日 下午3:27:09 
* @version V1.0   
*/
package com.yao.spring.cloud.weather.service;

import com.yao.spring.cloud.weather.vo.Weather;

/**
 * @Description: 天气预报服务
 * @author yaoym
 * @date 2018年3月23日 下午3:27:09
 * @version V1.0
 */
public interface WeatherReportService {

	/**
	 * 根据城市id查询天气数据
	 * 
	 * @Description:
	 * @since 2018年3月23日 下午3:28:33
	 * @author yaoym
	 * @param cityID
	 * @return
	 */
	Weather getDataByCityID(String cityID);
}
