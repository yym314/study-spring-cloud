package com.yao.spring.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yao.spring.cloud.service.CityClient;

@RestController
public class CityController {

	@Autowired
	private CityClient cityClient;

	@GetMapping(value = "/cities")
	public String listCity() {
		return cityClient.listCity();
	}
}
